#!/bin/bash

set -e

SCRIPT_NAME=$(basename "$0")

# Comprobar si se está ejecutando como superusuario (root)
if [ "$EUID" -ne 0 ]; then
    echo "Este script requiere permisos de superusuario. Por favor, ejecútalo con sudo:"
    echo "sudo $SCRIPT_NAME $*" # $SCRIPT_NAME es el nombre del script, $* son los argumentos pasados al script
    exit 1
fi

logger "Arrancando instalacion y configuracion de MongoDB"
USO="Uso : $SCRIPT_NAME [opciones]
Ejemplo: $SCRIPT_NAME -u administrador -p password [-n 27017]
Opciones:
-f archivo de configuración
-u usuario
-p password
-n numero de puerto (opcional)
-a muestra esta ayuda
"

MONGODB_VERSION="4.4"
MONGODB_COMPONENTS_VERSION="$MONGODB_VERSION.23"

ayuda() {
    echo "${USO}"
    if [[ ${1} ]]; then
        echo ${1}
    fi
}

readFile() {
    if [ ! -f "$1" ]; then
        echo "El archivo de configuración no existe: $1"
        exit 1
    fi

    FILE_CONTENT=$(cat $1)

    # Extraer el valor de 'user' y guardarlo en una variable
    MONGO_USER=$(echo "$FILE_CONTENT" | grep -oP '(?<=user=).*')
    MONGO_USER=$(echo "$MONGO_USER" | tr -d '\r')

    # Extraer el valor de 'password' y guardarlo en una variable
    MONGO_PASSWORD=$(echo "$FILE_CONTENT" | grep -oP '(?<=password=).*')
    MONGO_PASSWORD=$(echo "$MONGO_PASSWORD" | tr -d '\r')

    # Extraer el valor de 'port' y guardarlo en una variable
    MONGO_PORT=$(echo "$FILE_CONTENT" | grep -oP '(?<=port=).*')
    MONGO_PORT=$(echo "$MONGO_PORT" | tr -d '\r')

    # Imprimir los valores de las variables
    echo "Parametro MONGO_USER establecido con: $MONGO_USER"
    echo "Parametro MONGO_PASSWORD establecido"
    echo "Parametro MONGO_PORT establecido con ${MONGO_PORT}"
}

# Gestionar los argumentos
while getopts ":f:u:p:n:a" OPCION; do
    case ${OPCION} in
    f)
        readFile $OPTARG
        ;;
    u)
        MONGO_USER=$OPTARG
        echo "Parametro MONGO_USER establecido con '${MONGO_USER}'"
        ;;
    p)
        MONGO_PASSWORD=$OPTARG
        echo "Parametro MONGO_PASSWORD establecido"
        ;;
    n)
        MONGO_PORT=$OPTARG
        echo "Parametro MONGO_PORT establecido con '${MONGO_PORT}'"
        ;;
    a)
        ayuda
        exit 0
        ;;
    :)
        ayuda "Falta el parametro para -$OPTARG"
        exit 1
        ;;
    \?)
        ayuda "La opcion no existe : $OPTARG"
        exit 1
        ;;
    esac
done

if [ -z ${MONGO_USER} ]; then
    ayuda "El usuario debe ser especificado"
    exit 1
fi

if [ -z ${MONGO_PASSWORD} ]; then
    ayuda "La password debe ser especificada"
    exit 1
fi

if [ -z ${MONGO_PORT} ]; then
    MONGO_PORT=27017
fi

# Obtiene y valida la versión de ubuntu
UBUNTU_VERSION=$(lsb_release -cs)
UBUNTU_ALLOW_VERSIONS=("xenial" "bionic" "focal" "jammy")

if [[ " ${UBUNTU_ALLOW_VERSIONS[*]} " != *" $UBUNTU_VERSION "* ]]; then
    echo "La versión de Ubuntu ($UBUNTU_VERSION) NO es compatible con MongoDB $MONGO_VERSION"
    exit 1
fi

# Procede con la instalación
ASC_FILE="server-$MONGODB_VERSION.asc"
GPG_FILE="/usr/share/keyrings/mongodb-server-$MONGODB_VERSION.gpg"
REP_FILE="/etc/apt/sources.list.d/mongodb-org-$MONGODB_VERSION.list"
REP_URL="https://repo.mongodb.org/apt/ubuntu $UBUNTU_VERSION/mongodb-org/$MONGODB_VERSION multiverse"

# Obtén la clave de MongoDB
wget -qO - https://pgp.mongodb.com/$ASC_FILE | gpg --dearmor | sudo tee $GPG_FILE >/dev/null

# Agregar el repositorio según la versión de Ubuntu detectada
echo "deb [ arch=amd64,arm64 signed-by=$GPG_FILE ] $REP_URL" | sudo tee $REP_FILE

if [[ -z "$(mongo --version 2>/dev/null | grep '$MONGODB_VERSION')" ]]; then
    # Instalar paquetes comunes, servidor, shell, balanceador de shards y herramientas
    sudo apt-get -y update && apt-get install -y \
        mongodb-org=$MONGODB_COMPONENTS_VERSION \
        mongodb-org-server=$MONGODB_COMPONENTS_VERSION \
        mongodb-org-shell=$MONGODB_COMPONENTS_VERSION \
        mongodb-org-mongos=$MONGODB_COMPONENTS_VERSION \
        mongodb-org-tools=$MONGODB_COMPONENTS_VERSION
fi

# Crear las carpetas de logs y datos con sus permisos
[[ -d "/datos/bd" ]] || mkdir -p -m 755 "/datos/bd"
[[ -d "/datos/log" ]] || mkdir -p -m 755 "/datos/log"

# Establecer el dueño y el grupo de las carpetas db y log
chown mongodb /datos/log /datos/bd
chgrp mongodb /datos/log /datos/bd


# Respalda configuración original
mv /etc/mongod.conf /etc/mongod.conf.orig
# Crear el archivo de configuración de mongodb con el puerto solicitado
(cat << EOF 
# /etc/mongod.conf
# http://docs.mongodb.org/manual/reference/configuration-options/

# Where and how to store data.
storage:
  dbPath: /datos/bd
  engine: wiredTiger
  journal:
    enabled: true

# where to write logging data.
systemLog:
  destination: file
  logAppend: true
  path: /datos/log/mongod.log

# network interfaces
net:
  port: ${MONGO_PORT}
  bindIp: 127.0.0.1

# how the process runs
processManagement:
  timeZoneInfo: /usr/share/zoneinfo

EOF
) > /etc/mongod.conf


# Reiniciar el servicio de mongod para aplicar la nueva configuracion
systemctl restart mongod

# Tiempo máximo de espera en segundos
max_wait_time=15
# Tiempo de espera entre comprobaciones en segundos
wait_interval=1
elapsed_time=0
while true; do # Espera indefinidamente hasta que els ervicio este activo
    echo "Esperando por el servicio de mongod..."

    sleep $wait_interval
    elapsed_time=$((elapsed_time + wait_interval))
    
    if systemctl is-active --quiet mongod; then
        echo "El servicio 'mongod' se encuentra activo."
        break
    fi  

    if [ $elapsed_time -ge $max_wait_time ]; then
        echo "El servicio 'mongod' no se ha hecho disponible dentro del tiempo máximo de espera."
        exit 1
    fi
done

# Crear usuario con la password proporcionada como parametro
mongo admin << EOF
db.createUser(
    {
        user: "$MONGO_USER", 
        pwd: "$MONGO_PASSWORD",
        roles:[
            {role: "root", db: "admin"},
            {role: "restore",db: "admin"}
        ] 
    }
)
EOF

logger "El usuario ${MONGO_USER} ha sido creado con exito!"

exit 0

# user masterdevops
# db.document1.insert({"SampleValue1" : 255, "SampleValue2" : "randomStringOfText"})
