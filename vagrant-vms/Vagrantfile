# -*- mode: ruby -*-
# vi: set ft=ruby :
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.box = "ubuntu/focal64"
  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
    vb.memory = 4096
    vb.cpus = 2
    # This is commented out until resolution has been found for proper
    # usage. For now it will use the default graphics controller.
    vb.customize ['modifyvm', :id, '--graphicscontroller', 'vmsvga']
    vb.customize ['modifyvm', :id, '--audio', 'none']
  end


  # Common installs
  config.vm.provision "shell", inline: <<-SHELL
    # Hosts
    sudo echo "10.0.0.10 mongodb-server" >> /etc/hosts
    sudo apt-get update -y && sudo apt-get upgrade -y
    sudo timedatectl set-timezone America/Mexico_City
  SHELL

  
  # Mongo DB
  config.vm.define "mongodb-server" do |config|
    config.vm.provider "virtualbox" do |vb|
      vb.gui = true
    end

    config.vm.box_check_update = false
    config.vm.hostname = "mongodb-server"
    config.vm.network "private_network", ip: "10.0.0.10"
    config.vm.provision "file", source: "install-mongodb.sh", destination: "$HOME/"
    config.vm.provision "file", source: "uninstall-mongodb.sh", destination: "$HOME/"
    config.vm.provision "file", source: "config.ini", destination: "$HOME/"    
    config.vm.provision "shell", privileged: false, inline: <<-SHELL
      ulimit -n 64000
      # Keyboard config
      echo "setxkbmap -layout latam,es" >> $HOME/.profile
      
      # Mongodb script
      chmod +x install-mongodb.sh
      chmod +x uninstall-mongodb.sh
    SHELL

    config.vm.provision "shell", inline: <<-SHELL      
      # Add Repositories Packages
      wget -q -O - https://packages.microsoft.com/keys/microsoft.asc|sudo apt-key add -
      sudo sh -c 'echo \"deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main\" > /etc/apt/sources.list.d/microsoft.list'

      sudo apt-get update -y && sudo apt-get upgrade -y
      
      # Add desktop environment
      sudo apt-get install -y --no-install-recommends ubuntu-desktop
      sudo apt-get install -y --no-install-recommends virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11

      # Install Visual Studio Code
      sudo apt-get install -y code

      # Install robo 3t
      sudo apt-get install snapd
      sudo snap install robo3t-snap  
      
      # Restart VM
      sudo shutdown -r now
    SHELL
  end

end


# https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-ubuntu/
# https://ostechnix.com/install-mongodb-using-vagrant-in-linux/
# sudo apt-get install libmkl-dev libmkl-avx
# bcdedit /set xsavedisable 1
# bcdedit /set xsavedisable 0

# wget https://downloads.mongodb.com/compass/mongodb-compass_1.39.0_amd64.deb
# sudo dpkg -i mongodb-compass_1.39.0_amd64.deb

# wget https://downloads.mongodb.com/compass/mongodb-compass_1.15.1_amd64.deb
# sudo dpkg -i mongodb-compass_1.15.1_amd64.deb
# sudo apt --fix-broken install
# sudo apt -y install libgconf2-4