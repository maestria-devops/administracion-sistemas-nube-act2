#!/bin/bash

set -e

SCRIPT_NAME=$(basename "$0")

# Comprobar si se está ejecutando como superusuario (root)
if [ "$EUID" -ne 0 ]; then
    echo "Este script requiere permisos de superusuario. Por favor, ejecútalo con sudo:"
    echo "sudo $SCRIPT_NAME $*" # $SCRIPT_NAME es el nombre del script, $* son los argumentos pasados al script
    exit 1
fi

sudo service mongod stop

sudo apt-get purge mongodb-org*

sudo rm -r /datos

